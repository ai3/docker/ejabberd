#!/bin/sh
#
# Install script for ejabberd inside a Docker container
# (using Debian packages).
#

# Packages to install.
PACKAGES="
    ca-certificates
    ejabberd
    erlang-p1-mysql
    erlang-p1-pam
    libpam-authclient
    erlang-os-mon
    erlang-tools
    imagemagick

    python3
    python3-pip
    python3-setuptools
    python3-pymysql
"

# The default bitnami/minideb image defines an 'install_packages'
# command which is just a convenient helper. Define our own in
# case we are using some other Debian image.
if [ "x$(which install_packages)" = "x" ]; then
    install_packages() {
        env DEBIAN_FRONTEND=noninteractive apt-get install -qy -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --no-install-recommends "$@"
    }
fi

set -x
set -e

# Install the packages.
echo "deb [signed-by=/usr/share/keyrings/deb.autistici.org.gpg] http://deb.autistici.org/urepo float/bullseye/" \
    > /etc/apt/sources.list.d/ai3.list
apt-get -q update
install_packages ${PACKAGES}

# Remove requirement for a specific ejabberd user.
sed -i -e 's/^\(INSTALLUSER=\).*$/\1/' /usr/sbin/ejabberdctl

# Configure PAM.
cat >/etc/pam.d/ejabberd <<EOF
#%PAM-1.0
auth    sufficient      pam_authclient.so
account required        pam_authclient.so
EOF

# Install yoyo-migrations.
pip3 install yoyo-migrations

# Make sure start.sh is executable.
chmod 755 /usr/bin/start.sh

# Make sure /run has the right permissions.
chmod 1777 /run

apt-get autoremove -y
apt-get clean
rm -fr /var/lib/apt/lists/*
rm -fr /tmp/conf
