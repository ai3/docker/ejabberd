#!/bin/sh

# Sanity checks.
if [ ! -e /etc/ejabberd/ejabberd.yml ]; then
    echo "ERROR: ejabberd.yml not found" >&2
    exit 1
fi

# Create the rundir.
mkdir -p /run/ejabberd

# Run the database migrations. We get the SQL connection
# parameters by "parsing" the ejabberd.yml configuration.
get_config_value() {
    local key="$1"
    awk -F: "/^${key}:/ {print \$2}" /etc/ejabberd/ejabberd.yml | sed -e 's/^ *//' | sed -e 's/^"\(.*\)"$/\1/'
}

sql_server=$(get_config_value sql_server)
sql_port=$(get_config_value sql_port)
sql_user=$(get_config_value sql_username)
sql_pass=$(get_config_value sql_password)
sql_database=$(get_config_value sql_database)

if [ -z "${sql_server}" -o -z "${sql_port}" -o -z "${sql_user}" -o -z "${sql_pass}" -o -z "${sql_database}" ]; then
    echo: "ERROR: could not parse database configuration from ejabberd.yml" >&2
    exit 1
fi

echo "Applying migrations (mysql://${sql_user}:PASSWORD@${sql_server}:${sql_port}/${sql_database})"
/usr/local/bin/yoyo apply \
    --batch \
    --no-config-file \
    --database mysql://${sql_user}:${sql_pass}@${sql_server}:${sql_port}/${sql_database} \
    /migrations

if [ $? -gt 0 ]; then
    echo "ERROR: database migrations failed" >&2
    exit 1
fi

# Start ejabberd.
echo "Starting ejabberd"
exec /usr/sbin/ejabberdctl foreground

