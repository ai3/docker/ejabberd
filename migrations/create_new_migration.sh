#!/usr/bin/env bash

set -x

VERS_FILE=$PWD/.version

if [ $# -ne 2 ]; then
    echo "$0: <curr_version> <dest_version>"
    exit 1
fi

FROM=${1}
TO=${2}
if [[ -f ${VERS_FILE} ]]; then
    _PROG=$(cat ${VERS_FILE})
    PROG="$(printf "%04d" $(( $_PROG+1 )))"
else
    PROG="0001"
fi

echo $PROG > .version

${EDITOR:-vi} ${PROG}-${FROM}_to_${TO}.sql
