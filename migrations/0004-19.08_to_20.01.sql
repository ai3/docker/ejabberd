CREATE TABLE oauth_client (
       client_id varchar(191) NOT NULL PRIMARY KEY,
       client_name text NOT NULL,
       grant_type text NOT NULL,
       options text NOT NULL
)
