FROM debian:stable-slim

COPY build.sh /tmp/build.sh
COPY start.sh /usr/bin/start.sh
COPY deb_autistici_org.gpg /usr/share/keyrings/deb.autistici.org.gpg
COPY migrations/ /migrations/

RUN /tmp/build.sh && rm /tmp/build.sh

ENV HOME=/var/lib/ejabberd
ENTRYPOINT ["/usr/bin/start.sh"]

